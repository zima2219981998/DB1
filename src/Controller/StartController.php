<?php

namespace App\Controller;

use App\Entity\Register;
use App\Form\Type\RegisterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StartController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/push", name="push")
     * @Template
     */
    public function pushAction(Request $request)
    {
        $Register = new Register();

        $form = $this->createForm(RegisterType::class, $Register);

        $form->handleRequest($request);
        if($request->isMethod('POST')) {
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($Register);
                $em->flush();
                $form->getData();
                return $this->redirect($this->generateUrl('select'));
            }
        }

        return array(
            'form' => isset($form) ? $form->createView() : NULL
        );
    }

    /**
     * @Route("/select", name="select")
     * @Template
     */
    public function selectAction()
    {
        $repo = $this->getDoctrine()->getRepository('App:Register')->findAll();

        return array(
            'rows' => $repo
        );
    }

}